#!/usr/bin/env bash
# Copyright 2018-2023, Collabora, Ltd. and the Monado contributors
# SPDX-License-Identifier: BSL-1.0

##
#######################################################
#                GENERATED - DO NOT EDIT              #
# see .gitlab-ci/install-ndk.sh.jinja instead #
#######################################################
##


set -eo pipefail
# aka 25.2.9519653
RELEASE=${RELEASE:-r25c}
FN=android-ndk-${RELEASE}-linux.zip
wget "https://dl.google.com/android/repository/$FN"
unzip "$FN" -d /opt
mv "/opt/android-ndk-${RELEASE}" /opt/android-ndk

