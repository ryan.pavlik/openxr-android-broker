# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2022 Collabora, Ltd. and the Monado contributors
#
# To generate all the templated files, run this from the root of the repo:
#   make -f .gitlab-ci/ci-scripts.mk

# These also all have their template named the same with a .jinja suffix.
FILES_IN_SUBDIR := \
	.gitlab-ci/install-android-sdk.sh \
	.gitlab-ci/install-ndk.sh \
	.gitlab-ci/containers.yml \

CONFIG_FILE := .gitlab-ci/config.yml

OUTPUTS := \
    $(FILES_IN_SUBDIR)

all: $(OUTPUTS)
	chmod +x .gitlab-ci/*.sh
.PHONY: all

clean:
	rm -f $(OUTPUTS)
.PHONY: clean

CI_FAIRY := ci-fairy generate-template --config=$(CONFIG_FILE)

# Everything else is structured alike - we aren't generating the main CI script here
$(FILES_IN_SUBDIR): %: %.jinja $(CONFIG_FILE)
	$(CI_FAIRY) $< > $@
